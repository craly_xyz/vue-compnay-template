export default {
    /**
     * @description 配置显示在浏览器标签的title
     */
    title: '语麟信息科技',

    /** 公司名称 */
    name: '上海语麟信息科技有限公司',
    /** 公司微信 */
    wechat: 'GZWjhsmart',
    /** 公司邮箱 */
    email: 'wjhsmarter@126.com',
    /** 公司手机 */
    phone: '18279700225',
    /** 公司电话 */
    tel: '021-18279700225',
    /** 公司地址 */
    address: '浦东新区崂山路300号普联大厦1号楼5楼703',
    /** 备案号 */
    keepOnRecordIcp: '沪ICP备18041627号',

    /** 百度地图相关信息 */
    BDMapInfo: {
        longitude: 121.526896,
        latitude: 31.24022,
        title: '普联大厦',
        address: '浦东新区崂山路300号普联大厦1号楼5楼703'
    }
}